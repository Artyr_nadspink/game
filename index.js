(function (d) {

    player = true;


    d.onclick = function (e) {
        if ((e.target.className == "cell")) {
            if (player) {
                player = false;
                e.target.classList.add("ch");
            } else {
                e.target.classList.add("r");
                player = true;
            }
            var b0 = document.getElementById('c-0'), b1 = document.getElementById('c-1'),
                b2 = document.getElementById('c-2'), b3 = document.getElementById('c-3'),
                b4 = document.getElementById('c-4'), b5 = document.getElementById('c-5'),
                b6 = document.getElementById('c-6'), b7 = document.getElementById('c-7'),
                b8 = document.getElementById('c-8'), message = document.getElementsByClassName("won-message");


            if (b0.classList.contains("ch") && b1.classList.contains("ch") && b2.classList.contains("ch")) {
                document.getElementsByClassName("won-message")[0].innerHTML = "Crosses won!";
                document.getElementsByClassName("won-title")[0].classList.remove("hidden");
                document.getElementById('c-0').classList.add("win", "horizontal");
                document.getElementById('c-1').classList.add("win", "horizontal");
                document.getElementById('c-2').classList.add("win", "horizontal");

            } else if (b3.classList.contains("ch") && b4.classList.contains("ch") && b5.classList.contains("ch")) {
                document.getElementById('c-3').classList.add("win", "horizontal");
                document.getElementById('c-4').classList.add("win", "horizontal");
                document.getElementById('c-5').classList.add("win", "horizontal");
                document.getElementsByClassName("won-message")[0].innerHTML = "Crosses won!";
                document.getElementsByClassName("won-title")[0].classList.remove("hidden");
            } else if (b6.classList.contains("ch") && b7.classList.contains("ch") && b8.classList.contains("ch")) {
                document.getElementById('c-6').classList.add("win", "horizontal");
                document.getElementById('c-7').classList.add("win", "horizontal");
                document.getElementById('c-8').classList.add("win", "horizontal");
                document.getElementsByClassName("won-message")[0].innerHTML = "Crosses won!";
                document.getElementsByClassName("won-title")[0].classList.remove("hidden");

            } else if (b0.classList.contains("ch") && b3.classList.contains("ch") && b6.classList.contains("ch")) {
                document.getElementById('c-0').classList.add("win", "vertical");
                document.getElementById('c-3').classList.add("win", "vertical");
                document.getElementById('c-6').classList.add("win", "vertical");
                document.getElementsByClassName("won-message")[0].innerHTML = "Crosses won!";
                document.getElementsByClassName("won-title")[0].classList.remove("hidden");

            } else if (b1.classList.contains("ch") && b4.classList.contains("ch") && b7.classList.contains("ch")) {
                document.getElementById('c-1').classList.add("win", "vertical");
                document.getElementById('c-4').classList.add("win", "vertical");
                document.getElementById('c-7').classList.add("win", "vertical");
                document.getElementsByClassName("won-message")[0].innerHTML = "Crosses won!";
                document.getElementsByClassName("won-title")[0].classList.remove("hidden");
            }
            else if (b2.classList.contains("ch") && b5.classList.contains("ch") && b8.classList.contains("ch")) {
                document.getElementById('c-2').classList.add("win", "vertical");
                document.getElementById('c-5').classList.add("win", "vertical");
                document.getElementById('c-8').classList.add("win", "vertical");
                document.getElementsByClassName("won-message")[0].innerHTML = "Crosses won!";
                document.getElementsByClassName("won-title")[0].classList.remove("hidden");
            }
            else if (b0.classList.contains("r") && b3.classList.contains("r") && b6.classList.contains("r")) {
                document.getElementById('c-0').classList.add("win", "vertical");
                document.getElementById('c-3').classList.add("win", "vertical");
                document.getElementById('c-6').classList.add("win", "vertical");
                document.getElementsByClassName("won-message")[0].innerHTML = "Toes won!";
                document.getElementsByClassName("won-title")[0].classList.remove("hidden");


            } else if (b1.classList.contains("r") && b4.classList.contains("r") && b7.classList.contains("r")) {
                document.getElementById('c-1').classList.add("win", "vertical");
                document.getElementById('c-4').classList.add("win", "vertical");
                document.getElementById('c-7').classList.add("win", "vertical");
                document.getElementsByClassName("won-message")[0].innerHTML = "Toes won!";
                document.getElementsByClassName("won-title")[0].classList.remove("hidden");
            }
            else if (b2.classList.contains("r") && b5.classList.contains("r") && b8.classList.contains("r")) {
                document.getElementById('c-2').classList.add("win", "vertical");
                document.getElementById('c-5').classList.add("win", "vertical");
                document.getElementById('c-8').classList.add("win", "vertical");
                document.getElementsByClassName("won-message")[0].innerHTML = "Toes won!";
                document.getElementsByClassName("won-title")[0].classList.remove("hidden");
            }
            else if (b0.classList.contains("r") && b1.classList.contains("r") && b2.classList.contains("r")) {
                document.getElementsByClassName("won-message")[0].innerHTML = "Toes won!";
                document.getElementsByClassName("won-title")[0].classList.remove("hidden");
                document.getElementById('c-0').classList.add("win", "horizontal");
                document.getElementById('c-1').classList.add("win", "horizontal");
                document.getElementById('c-2').classList.add("win", "horizontal");
            } else if (b3.classList.contains("r") && b4.classList.contains("r") && b5.classList.contains("r")) {
                document.getElementById('c-3').classList.add("win", "horizontal");
                document.getElementById('c-4').classList.add("win", "horizontal");
                document.getElementById('c-5').classList.add("win", "horizontal");
                document.getElementsByClassName("won-message")[0].innerHTML = "Toes won!";
                document.getElementsByClassName("won-title")[0].classList.remove("hidden");
            } else if (

                b6.classList.contains("r") && b7.classList.contains("r") && b8.classList.contains("r")
            ) {
                document.getElementById('c-6').classList.add("win", "horizontal");
                document.getElementById('c-7').classList.add("win", "horizontal");
                document.getElementById('c-8').classList.add("win", "horizontal");
                document.getElementsByClassName("won-message")[0].innerHTML = "Toes won!";
                document.getElementsByClassName("won-title")[0].classList.remove("hidden");

            }
            else if ((b0.classList.contains("ch") || b0.classList.contains("r")) && (b1.classList.contains("ch") || b1.classList.contains("r"))
                && (b2.classList.contains("ch") || b2.classList.contains("r")) && (b3.classList.contains("ch") || b3.classList.contains("r"))
                && (b4.classList.contains("ch") || b4.classList.contains("r")) && (b5.classList.contains("ch") || b5.classList.contains("r"))
                && (b6.classList.contains("ch") || b6.classList.contains("r")) && (b7.classList.contains("ch") || b7.classList.contains("r"))
                && (b8.classList.contains("ch") || b8.classList.contains("r"))
            ) {

                document.getElementsByClassName("won-message")[0].innerHTML = "It's a draw!";
                document.getElementsByClassName("won-title")[0].classList.remove("hidden");
            }
            // var arr = d.querySelectorAll('.cell'), v = [];
            // for (var i = 0, len = arr.length; i < len; i++) {
            //     v[i] = arr[i].innerText
            // }
            //
            //     if ((v[0] == "X" && v[1] == "X" && v[2] == "X") || (v[3] == "X" && v[4] == "X" && v[5] == "X") || (v[6] == "X" && v[7] == "X" && v[8] == "X") || (v[0] == "X" && v[3] == "X" && v[6] == "X") || (v[1] == "X" && v[4] == "X" && v[7] == "X") || (v[2] == "X" && v[5] == "X" && v[8] == "X") || (v[0] == "X" && v[4] == "X" && v[8] == "X") || (v[2] == "X" && v[4] == "X" && v[6] == "X")) {
            //     alert("X wins!");
            // }
            // if ((v[0] == "O" && v[1] == "O" && v[2] == "O") || (v[3] == "O" && v[4] == "O" && v[5] == "O") || (v[6] == "O" && v[7] == "O" && v[8] == "O") || (v[0] == "O" && v[3] == "O" && v[6] == "O") || (v[1] == "O" && v[4] == "O" && v[7] == "O") || (v[2] == "O" && v[5] == "O" && v[8] == "O") || (v[0] == "O" && v[4] == "O" && v[8] == "O") || (v[2] == "O" && v[4] == "O" && v[6] == "O")) {
            //     alert("O wins!");
            // }
        }
    };
})(document);
